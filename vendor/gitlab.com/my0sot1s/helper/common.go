package helper

import (
	"encoding/json"
	"errors"
	"fmt"
)

func ConvInterface2String(i interface{}) string {
	switch i.(type) {
	case string:
		return fmt.Sprintf("%v", i)
	default:
		return ""
	}
}

func Contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}

func Str2T(str string, T interface{}) error {
	if str == "" {
		return errors.New("str empty")
	}
	return json.Unmarshal([]byte(str), &T)
}

func ErrStr(err error) string {
	return fmt.Sprintf("%v", err)
}

func ConveterM2I(in, out interface{}) error {
	bin, err := json.Marshal(in)
	if err != nil {
		return err
	}
	return json.Unmarshal(bin, &out)
}

func ConveterI2M(in interface{}, out M) error {
	bin, err := json.Marshal(in)
	if err != nil {
		return nil
	}
	return json.Unmarshal(bin, &out)
}
