# Account - 5Service
### Service quản lý account của hệ thống. [document here](https://swagger.tenguyen.tk)
<br>

## Các step trong hệ thống.
1. Tạo một người dùng.
	Mỗi một người dùng được tạo thành 1 bản ghi trong hệ thống. Được tham chiếu tới đối tượng account.User. Có dạng như sau.
	```json
		{
			"id": "string",
			"fullname": "string",
			"email": "string",
			"phone": "string",
			"address": "string",
			"country": "string",
			"city": "string",
			"province": "string",
			"apartment": "string",
			"village": "string",
			"birthday": "string",
			"gender": "string",
			"created": "string",
			"state": "string"
		}
	```
	Mỗi một user là 1 đinh danh của 1 cá nhân duy nhất trong hệ thống. Được xác định qua email hoặc số điện thoại xác định.
2. Người dùng dùng dịch vụ của 5Service
	- Thông tin người dùng được lựa chọn để lưu trữ tại đại dữ liệu của doanh nghiệp. Sau lúc tạo hóa đơn.
	- Được update khi có yêu cầu và được cá nhân đồng ý.
3. Người dùng trở thành tạo mới branch.
	- Người dùng gửi yêu cầu trở tạo mới 1 branch lên hệ thống 5service. Người dùng có thể phải gửi các ảnh và điền các thông tin và điều khoản dịch vụ. Nhân viên của hệ thống 5Servicce tiến hành review bằng ảnh và gọi điện.
	- Hệ thống kiểm tra yêu cầu. Tiến hành review các điều kiện. Nếu yêu cầu đc xác thực thì tiến hành cho phép tạo branch.
	- Hệ thống tiến hành convert `user` đó thành  `employee` với permission là owner. Full các permission. Đồng thời tiến hành bắt ng dùng điền form tạo mới branch.
		```json
			{
				"id": "string",
				"name": "string",
				"phone": "string",
				"description": "string",
				"trace": {
					"ip": "string",
					"country_code": "string",
					"country_name": "string",
					"region_code": "string",
					"region_name": "string",
					"latitude": "string",
					"longitude": "string"
				},
				"avatar_url": "string",
				"city": "string",
				"district": "string",
				"province": "string",
				"attributes": [
					{
						"key": "string",
						"value": "string",
						"values": [
							"string"
						]
					}
				],
				"created": "string",
				"state": "string"
			}
		```
	- Sau bước này có thể thêm nhân viên vào hệ thống. Hoặc nhân viên tự apply vào trở thành employee.
4. Người dùng được muốn là nhân viên.
	- Người dùng gửi yêu cầu trở thành nhân viên của hệ thống.
	- Người dùng đó sau khi điền đủ thông tin biểu mẫu. Các thông tin sẽ được lưu lại với bản ghi của doanh nghiệp với đối tượng account.Employee
		```json
		{
			"id": "string",
			"name": "string",
			"branch_id": "string",
			"Branch": "string",
			"email": "string",
			"password": "string",
			"is_owner": true,
			"permissions": {
				"perm_id": true,
			},
			"started": "string",
			"ended": "string",
			"created": "string",
			"state": "string"
		}
		```
	- Thông tin nhân viên được accept bởi chủ (owner) thì trở thành nhân viên của branch. Được cấp các quyền cơ bản nhất
5. Tiến hành thêm service vào hệ thống.
	- Nhân viên branch có thể thêm các service bằng bấm `add service` trên giao diện.
	- Nhân viên hoàn thành biểu mẫu và tiến hành tạo bản ghi `service`
		```json
		{
			"id": "string",
			"branch_id": "string",
			"branch": {
				"id": "string",
				"name": "string",
				"phone": "string",
				"description": "string",
				"trace": {
					"ip": "string",
					"country_code": "string",
					"country_name": "string",
					"region_code": "string",
					"region_name": "string",
					"latitude": "string",
					"longitude": "string"
				},
				"avatar_url": "string",
				"city": "string",
				"district": "string",
				"province": "string",
				"attributes": [
					{
						"key": "string",
						"value": "string",
						"values": [
							"string"
						]
					}
				],
				"created": "string",
				"state": "string"
			},
			"name": "string",
			"min_price": "string",
			"max_price": "string",
			"description": "string",
			"seat": 0,
			"average_time": 0,
			"created": "string",
			"images": [
				{
					"url": "string",
					"size": "string",
					"thumb_url": "string",
					"type": "string"
				}
			],
			"state": "string"
		}
		```
	[còn nữa ...]()