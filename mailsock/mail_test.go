package mailsock

import (
	"testing"

	"github.com/my0sot1s/godef/log"
)

func Test_SendEmail(t *testing.T) {
	gun := &MailSock{}
	gun.InitEmail(
		"8e301c6729e6c6fc6578409697238604-b0aac6d0-4e6e81cd",
		"sandboxa15e686609f145c3b31aabefeb67937a.mailgun.org",
	)
	res, id, _ := gun.SendEmail(&Mail{
		From:    "Mailgun Sandbox <test@sandboxa15e686609f145c3b31aabefeb67937a.mailgun.org>",
		Subject: "Sent test done",
		Text:    "Congratulations Te Nguyen, you just sent an email with Mailgun!  You are truly awesome!",
		To:      "Te Nguyen <manhte231@gmail.com>",
	})
	log.Log(string(res), id)
}
