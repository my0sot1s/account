package mailsock

import (
	mailgun "github.com/mailgun/mailgun-go"
)

// Mail email struct
type Mail struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Subject string `json:"subject"`
	Text    string `json:"text"`
}

// MailSock email struct
type MailSock struct {
	mailCon *mailgun.MailgunImpl
	apiKey  string
	domain  string
}

// InitEmail init add config
func (m *MailSock) InitEmail(apiKey, host string) {
	m.apiKey = apiKey
	m.domain = "noreply@" + host
	m.mailCon = mailgun.NewMailgun(m.domain, m.apiKey)
}

// SendEmail send email
func (m *MailSock) SendEmail(mail *Mail) ([]byte, string, error) {
	message := m.mailCon.NewMessage(mail.From, mail.Subject, mail.Text, mail.To)
	resp, id, err := m.mailCon.Send(message)
	return []byte(resp), id, err
}
