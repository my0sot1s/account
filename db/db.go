package db

import (
	"errors"

	"gitlab.com/my0sot1s/header/account"
	"gitlab.com/my0sot1s/helper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type DB struct {
	db  *mgo.Database
	url string
}

const (
	Account  = "Account"
	Employee = "Employee"
	Branch   = "Branch"
)

func (d *DB) InitDB(dbName, url string) error {
	session, err := mgo.Dial(url)
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)
	d.db = session.DB(dbName)
	return nil
}

// --------- Account ----------------
func (d *DB) ReadAccount(query *account.AccountRequest) (*account.Account, error) {
	acc, m := &account.Account{}, make(helper.M)
	if query.GetId() != "" {
		if !bson.IsObjectIdHex(query.GetId()) {
			return nil, errors.New("id is not hex")
		}
		if err := d.db.C(Account).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &acc)
		return acc, err
	} else if query.GetEmail() != "" {
		if err := d.db.C(Account).Find(bson.M{"email": query.GetEmail()}).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &acc)
		return acc, err
	}
	return nil, errors.New("not have id or email")
}
func (d *DB) ReadAccounts(query *account.AccountRequest) ([]*account.Account, error) {
	m, accounts := make([]helper.M, 0), make([]*account.Account, 0)
	anchor, limit := query.GetAnchor(), int(query.GetLimit())
	if err := d.db.C(Account).Find(limitAnchor(limit, anchor)).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &accounts)
	return accounts, err
}

func (d *DB) UpsertAccount(acc *account.Account) (*account.Account, error) {
	if !bson.IsObjectIdHex(acc.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(acc, m)
		m["_id"], acc.Id = bid, bid.Hex()
		return acc, d.db.C(Account).Insert(m)
	}
	acc.Created = 0
	m := make(bson.M)
	helper.ConveterI2M(acc, m)
	err := d.db.C(Account).UpdateId(bson.ObjectIdHex(acc.GetId()), bson.M{"$set": m})
	return acc, err
}

func (d *DB) DeleteAccount(id string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(Account).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

// -------------------- Employee ---------------
func (d *DB) ReadEmployee(query *account.EmployeeRequest) (*account.Employee, error) {
	employee, m := &account.Employee{}, make(helper.M)
	if query.GetId() != "" {
		if !bson.IsObjectIdHex(query.GetId()) {
			return nil, errors.New("id is not hex")
		}
		if err := d.db.C(Employee).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &employee)
		return employee, err
	} else if query.GetEmail() != "" {
		if err := d.db.C(Employee).Find(bson.M{"email": query.GetEmail()}).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &employee)
		return employee, err
	}
	return nil, errors.New("not have id or email")
}

func (d *DB) ReadEmployees(query *account.EmployeeRequest) ([]*account.Employee, error) {
	m, units := make([]helper.M, 0), make([]*account.Employee, 0)
	anchor, limit := query.GetAnchor(), int(query.GetLimit())
	if err := d.db.C(Employee).Find(limitAnchor(limit, anchor)).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &units)
	return units, err
}

func (d *DB) UpsertEmployee(em *account.Employee) (*account.Employee, error) {
	if !bson.IsObjectIdHex(em.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(em, m)
		m["_id"], em.Id = bid, bid.Hex()
		return em, d.db.C(Employee).Insert(m)
	}
	m := make(bson.M)
	helper.ConveterI2M(em, m)
	return em, d.db.C(Employee).UpdateId(bson.ObjectIdHex(em.GetId()), bson.M{"$set": m})
}

func (d *DB) DeleteEmployee(id string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(Employee).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

// -------------------- Branch ---------------
func (d *DB) ReadBranch(query *account.BranchRequest) (*account.Branch, error) {
	if !bson.IsObjectIdHex(query.GetId()) {
		return nil, errors.New("id is not hex")
	}
	branch, m := &account.Branch{}, make(helper.M)
	if err := d.db.C(Branch).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtId(m), &branch)
	return branch, err
}

func (d *DB) ReadBranches(query *account.BranchRequest) ([]*account.Branch, error) {
	m, units := make([]helper.M, 0), make([]*account.Branch, 0)
	anchor, limit := query.GetAnchor(), int(query.GetLimit())
	if err := d.db.C(Branch).Find(limitAnchor(limit, anchor)).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &units)
	return units, err
}

func (d *DB) UpsertBranch(b *account.Branch) (*account.Branch, error) {
	if !bson.IsObjectIdHex(b.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(b, m)
		m["_id"], b.Id = bid, bid.Hex()
		return b, d.db.C(Branch).Insert(m)
	}
	m := make(bson.M)
	helper.ConveterI2M(b, m)
	return b, d.db.C(Branch).UpdateId(bson.ObjectIdHex(b.GetId()), bson.M{"$set": m})
}

func (d *DB) DeleteBranch(id string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(Branch).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}
