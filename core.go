package main

import (
	"context"
	"errors"
	"time"

	"gitlab.com/my0sot1s/account/mailsock"

	"gitlab.com/my0sot1s/auth/hashing"

	"gitlab.com/my0sot1s/header/account"
	"gitlab.com/my0sot1s/header/common"
)

type Account struct {
	db      IDB
	cache   ICache
	search  ISearchMachine
	hash    *hashing.Hash
	mailGun *mailsock.MailSock
}

// CreateAccount is a func implement
func (acc *Account) CreateAccount(ctx context.Context, in *account.Account) (*account.Account, error) {
	in.State = account.Account_State_name[3]
	accountReq := &account.AccountRequest{Email: in.GetEmail()}
	if ac, err := acc.db.ReadAccount(accountReq); err == nil && ac.GetId() != "" {
		return nil, errors.New("account is existed")
	}
	in.Created = time.Now().UnixNano()
	ac, err := acc.db.UpsertAccount(in)
	if err == nil {
		go acc.search.AddIndexAccount(ac)
	}
	// insert db
	return ac, err
}

// UpdateAccount is a func implement
func (acc *Account) UpdateAccount(ctx context.Context, in *account.Account) (*account.Account, error) {
	accountReq := &account.AccountRequest{Email: in.GetEmail(), Id: in.GetId()}
	if ac, err := acc.db.ReadAccount(accountReq); err != nil && ac.GetId() == "" {
		return nil, errors.New("account is not existed")
	}
	// field can update
	ac, err := acc.db.UpsertAccount(&account.Account{
		Id:        in.GetId(),
		BannerUrl: in.GetBannerUrl(),
		AvatarUrl: in.GetAvatarUrl(),
		Name:      in.GetName(),
	})
	if err != nil {
		return nil, err
	}
	go acc.search.UpdateIndexAccount(ac)
	return acc.db.ReadAccount(&account.AccountRequest{Id: in.GetId()})
}

// ReadAccounts is a func implement
func (acc *Account) ReadAccounts(ctx context.Context, in *account.AccountRequest) (*account.AccountResponse, error) {
	req := &account.AccountRequest{Limit: -100, Anchor: in.GetAnchor()}
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	accounts, err := acc.db.ReadAccounts(req)
	if err != nil {
		return nil, err
	}
	resp := &account.AccountResponse{Anchor: "", Accounts: accounts}
	if len(accounts) == 0 {
		resp.Anchor = ""
	} else if req.GetLimit() > 0 {
		resp.Anchor = accounts[len(accounts)-1].GetId()
	} else {
		resp.Anchor = accounts[0].GetId()
	}
	return resp, nil
}

// SearchAccount is a func implement
func (acc *Account) SearchAccounts(ctx context.Context, in *account.AccountRequest) (*account.AccountResponse, error) {
	accounts, err := acc.search.SearchIndexAccounts(in)
	if err != nil {
		return nil, err
	}
	return &account.AccountResponse{
		Accounts: accounts,
	}, nil
}

// ReadAccount is a func implement
func (acc *Account) ReadAccount(ctx context.Context, in *account.AccountRequest) (*account.Account, error) {
	account, err := acc.db.ReadAccount(&account.AccountRequest{Id: in.GetId(), Email: in.GetEmail()})
	if err != nil {
		return nil, err
	}
	return account, nil
}

// CreateBranch is a func implement
func (acc *Account) CreateBranch(ctx context.Context, in *account.Branch) (*account.Branch, error) {
	in.State = account.Branch_State_name[0]
	accountReq := &account.AccountRequest{Id: in.GetAccountId()}
	if ac, err := acc.db.ReadAccount(accountReq); err != nil && ac.GetId() == "" {
		return nil, errors.New("account is not existed")
	}
	in.Created = time.Now().UnixNano()
	// insert db
	b, err := acc.db.UpsertBranch(in)
	if err != nil {
		go acc.search.AddIndexBranch(b)
	}
	return b, err
}

//UpdateBranch is a func implement
func (acc *Account) UpdateBranch(ctx context.Context, in *account.Branch) (*account.Branch, error) {
	if ac, err := acc.db.ReadAccount(&account.AccountRequest{Id: in.GetAccountId()}); err != nil && ac.GetId() == "" {
		return nil, errors.New("account is not existed")
	}
	if bra, err := acc.db.ReadBranch(&account.BranchRequest{Id: in.GetId()}); err != nil && bra.GetId() == "" {
		return nil, errors.New("branch is not existed")
	}
	// field can update
	b, err := acc.db.UpsertBranch(&account.Branch{
		Name:      in.GetName(),
		AvatarUrl: in.GetAvatarUrl(),
	})
	if err != nil {
		return nil, err
	}
	go acc.search.UpdateIndexBranch(b)
	return acc.db.ReadBranch(&account.BranchRequest{Id: in.GetId()})
}

//DeleteBranch is a func implement
func (acc *Account) DeleteBranch(ctx context.Context, in *account.BranchRequest) (*common.NIL, error) {
	defer acc.search.DeleteIndexBranch(in.GetId())
	return &common.NIL{}, acc.db.DeleteBranch(in.GetId())
}

// ReadBranches is a func implement
func (acc *Account) ReadBranches(ctx context.Context, in *account.BranchRequest) (*account.BranchResponse, error) {
	req := &account.BranchRequest{Limit: -100, Anchor: in.GetAnchor()}
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	branches, err := acc.db.ReadBranches(req)
	if err != nil {
		return nil, err
	}
	resp := &account.BranchResponse{Anchor: "", Branches: branches}
	if len(branches) == 0 {
		resp.Anchor = ""
	} else if req.GetLimit() > 0 {
		resp.Anchor = branches[len(branches)-1].GetId()
	} else {
		resp.Anchor = branches[0].GetId()
	}
	return resp, nil
}

// SearchBranchs is a func implement
func (acc *Account) SearchBranchs(ctx context.Context, in *account.BranchRequest) (*account.BranchResponse, error) {
	branches, err := acc.search.SearchIndexBranchs(in)
	if err != nil {
		return nil, err
	}
	return &account.BranchResponse{
		Branches: branches,
	}, nil
}

//ReadBranch is a func implement
func (acc *Account) ReadBranch(ctx context.Context, in *account.BranchRequest) (*account.Branch, error) {
	req := &account.BranchRequest{Id: in.GetId()}
	branch, err := acc.db.ReadBranch(req)
	if err != nil {
		return nil, err
	}
	return branch, nil
}

//CreateEmployee is a func implement
func (acc *Account) CreateEmployee(ctx context.Context, in *account.Employee) (*account.EmployeeResponse, error) {
	if em, err := acc.db.ReadEmployee(&account.EmployeeRequest{Email: in.GetEmail()}); err == nil && em.GetId() != "" {
		return nil, errors.New("employee is existed")
	}
	if ac, err := acc.db.ReadAccount(&account.AccountRequest{Id: in.GetAccountId()}); err != nil && ac.GetId() == "" {
		return nil, errors.New("account is not existed")
	}
	if br, err := acc.db.ReadBranch(&account.BranchRequest{Id: in.GetBranchId()}); err != nil && br.GetId() == "" {
		return nil, errors.New("branch is not existed")
	}
	in.State = account.Employee_State_name[1]
	in.Created = time.Now().UnixNano()
	in.Password = ""
	token := acc.hash.MakeRandomString(36)
	// setCache
	acc.cache.SetValue(token, in.GetEmail(), 48*time.Hour)
	// sent email
	acc.mailGun.SendEmail(&mailsock.Mail{
		From:    "noreply-5services@sandboxa15e686609f145c3b31aabefeb67937a.mailgun.org",
		To:      in.GetEmail(),
		Subject: "Email active employee, enjoy to join us company",
		Text: `Click this link to active agent :
		http://api.tenguyen.tk/invited?token=` + token,
	})
	// insert db
	em, err := acc.db.UpsertEmployee(in)
	return &account.EmployeeResponse{Token: token, Employee: em}, err
}

//UpdateEmployee is a func implement
func (acc *Account) UpdateEmployee(ctx context.Context, in *account.Employee) (*account.Employee, error) {
	if ac, err := acc.db.ReadAccount(&account.AccountRequest{Id: in.GetAccountId()}); err != nil && ac.GetId() == "" {
		return nil, errors.New("account is not existed")
	}
	if bra, err := acc.db.ReadBranch(&account.BranchRequest{Id: in.GetBranchId()}); err != nil && bra.GetId() == "" {
		return nil, errors.New("branch is not existed")
	}
	// field can update
	return acc.db.UpsertEmployee(&account.Employee{
		Name:      in.GetName(),
		AvatarUrl: in.GetAvatarUrl(),
		Email:     in.GetEmail(),
	})
}

//DeleteEmployee is a func implement
func (acc *Account) DeleteEmployee(ctx context.Context, in *account.EmployeeRequest) (*common.NIL, error) {
	return &common.NIL{}, acc.db.DeleteEmployee(in.GetId())
}

//ReadEmployees is a func implement
func (acc *Account) ReadEmployees(ctx context.Context, in *account.EmployeeRequest) (*account.EmployeeResponse, error) {
	req := &account.EmployeeRequest{Limit: -100, Anchor: in.GetAnchor()}
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	employees, err := acc.db.ReadEmployees(req)
	if err != nil {
		return nil, err
	}
	resp := &account.EmployeeResponse{Anchor: "", Employees: employees}
	if len(employees) == 0 {
		resp.Anchor = ""
	} else if req.GetLimit() > 0 {
		resp.Anchor = employees[len(employees)-1].GetId()
	} else {
		resp.Anchor = employees[0].GetId()
	}
	return resp, nil
}

//ReadEmployee is a func implement
func (acc *Account) ReadEmployee(ctx context.Context, in *account.EmployeeRequest) (*account.Employee, error) {
	req := &account.EmployeeRequest{Id: in.GetId()}
	employee, err := acc.db.ReadEmployee(req)
	if err != nil {
		return nil, err
	}
	return employee, nil
}

// InvitedEmployee is a func implement
func (acc *Account) InvitedEmployee(ctx context.Context, in *account.EmployeeRequest) (*account.Employee, error) {
	if in.GetToken() == "" {
		return nil, errors.New("token is empty")
	}
	// verify token
	payload, err := acc.cache.GetValue(in.GetToken())
	if payload == "" || err != nil || payload != in.GetEmail() {
		return nil, errors.New("token is invalid")
	}
	em := in.GetEmployee()
	if em.GetPassword() == "" || len(em.GetPassword()) < 6 {
		return nil, errors.New("password not found or so weak")
	}
	pw, _ := acc.hash.Generate(em.GetPassword())
	return acc.db.UpsertEmployee(&account.Employee{Password: pw})
}
