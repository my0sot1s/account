package es

import (
	"encoding/json"

	"github.com/olivere/elastic"
	"gitlab.com/my0sot1s/header/account"
)

const (
	_account = "esaccount"
	_type    = "_doc"
)

// SearchIndexAccounts ss
func (s *SearchMachine) SearchIndexAccounts(query *account.AccountRequest) ([]*account.Account, error) {
	esQuery := query.GetEsQuery()
	termQuery := elastic.NewTermQuery(esQuery.GetKey(), esQuery.GetQuery())
	size := int(esQuery.GetSize())
	if size == 0 {
		size = 40
	}
	searchResult, err := s.client.Search().
		Index(_account).
		Query(termQuery).
		From(int(esQuery.GetFrom())).
		Size(size).
		Do(s.ctx)
	if err != nil {
		return nil, err
	}
	accounts := make([]*account.Account, 0)
	if searchResult.Hits.TotalHits > 0 {
		// Iterate through results
		for _, hit := range searchResult.Hits.Hits {
			// hit.Index contains the name of the index
			var acc *account.Account
			// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
			err := json.Unmarshal(*hit.Source, &acc)
			if err == nil {
				accounts = append(accounts, acc)
			}
		}
	}
	return accounts, nil
}

// AddIndexAccount ss
func (s *SearchMachine) AddIndexAccount(account *account.Account) error {
	_, err := s.client.Index().
		Index(_account).
		Type(_type).
		Id(account.GetId()).
		BodyJson(account).
		Do(s.ctx)
	return err
}

// UpdateIndexAccount any
func (s *SearchMachine) UpdateIndexAccount(acc *account.Account) error {
	_, err := s.client.Update().
		Index(_branch).
		Type(_type).
		Id(acc.GetId()).
		Doc(&acc).Do(s.ctx)
	return err
}

// DeleteIndexAccount any
func (s *SearchMachine) DeleteIndexAccount(id string) error {
	_, err := s.client.Delete().
		Index(_account).
		Type(_type).
		Id(id).
		Do(s.ctx)
	return err
}
