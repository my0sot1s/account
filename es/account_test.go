package es

import (
	"testing"
	"time"

	"gitlab.com/my0sot1s/header/account"
	"gitlab.com/my0sot1s/header/common"
	"gitlab.com/my0sot1s/helper"
)

func Test_Add(t *testing.T) {
	search := &SearchMachine{}
	if err := search.InitElastic("https://1ho3a70ewf:o2nqvzcdq6@elastic-go-dev-2059688389.ap-southeast-2.bonsaisearch.net:443", "esbranch", "branch.json"); err != nil {
		helper.Log("xxxxxxxxx", err)
	}
	branchs := make([]*account.Branch, 0)
	branchs = append(branchs, &account.Branch{
		Id:        "asdasd",
		Name:      "minh abc",
		AccountId: "qwertyuiop",
		Created:   time.Now().Unix(),
	})
	if err := search.AddIndexBranchs(branchs); err != nil {
		helper.Log(err)
	}
}
func Test_Add2(t *testing.T) {
	search := &SearchMachine{}
	errs := search.InitElasticConfig("https://1ho3a70ewf:o2nqvzcdq6@elastic-go-dev-2059688389.ap-southeast-2.bonsaisearch.net:443", map[string]string{
		"esbranch":  "branch.json",
		"esaccount": "account.json",
	})
	if errs != nil {
		helper.Log("xxxxxxxxx", errs)
	}
	branchs := make([]*account.Branch, 0)
	branchs = append(branchs, &account.Branch{
		Id:        "aqweqeq",
		Name:      "minh ngoc",
		AccountId: "qwertyuiop",
		Created:   time.Now().Unix(),
	})
	branchs = append(branchs, &account.Branch{
		Id:        "adaffdf",
		Name:      "minh loan",
		AccountId: "qwertyuiop",
		Created:   time.Now().Unix(),
	})
	branchs = append(branchs, &account.Branch{
		Id:        "asdfasas",
		Name:      "min h asdasd",
		AccountId: "qwertyuiop",
		Created:   time.Now().Unix(),
	})
	branchs = append(branchs, &account.Branch{
		Id:        "asdfasassa",
		Name:      "Minh asdasd",
		AccountId: "qwertyuiop",
		Created:   time.Now().Unix(),
	})
	if err := search.AddIndexBranchs(branchs); err != nil {
		helper.Log(err)
	}
}

func Test_Search(t *testing.T) {
	search := &SearchMachine{}
	errs := search.InitElasticConfig("https://1ho3a70ewf:o2nqvzcdq6@elastic-go-dev-2059688389.ap-southeast-2.bonsaisearch.net:443", map[string]string{
		"esbranch":  "branch.json",
		"esaccount": "account.json",
	})
	if len(errs) > 0 {
		helper.Log("xxxxxxxxx", errs)
	}
	b, err := search.SearchIndexBranchs(&account.BranchRequest{
		EsQuery: &common.ESQuery{
			Key:   "name",
			Query: "minh",
			Size:  10,
		},
	})
	if err != nil {
		helper.Log("Errorrrrrrrrrr", err)
	}
	helper.Log(b)
}
func Test_update(t *testing.T) {
	search := &SearchMachine{}
	errs := search.InitElasticConfig("https://1ho3a70ewf:o2nqvzcdq6@elastic-go-dev-2059688389.ap-southeast-2.bonsaisearch.net:443", map[string]string{
		"esbranch":  "branch.json",
		"esaccount": "account.json",
	})
	if errs != nil {
		helper.Log("xxxxxxxxx", errs)
	}
	b := &account.Branch{
		Id:   "adaffdf",
		Name: "minh loan 3",
	}
	if err := search.UpdateIndexBranch(b); err != nil {
		helper.Log(err)
	}
}
