package es

import (
	"encoding/json"

	"github.com/olivere/elastic"
	"gitlab.com/my0sot1s/header/account"
)

const (
	_branch = "esbranch"
)

// SearchIndexBranchs ss
func (s *SearchMachine) SearchIndexBranchs(query *account.BranchRequest) ([]*account.Branch, error) {
	esQuery := query.GetEsQuery()
	termQuery := elastic.NewTermQuery(esQuery.GetKey(), esQuery.GetQuery())
	size := int(esQuery.GetSize())
	if size == 0 {
		size = 40
	}
	searchResult, err := s.client.Search().
		Index(_branch).
		Query(termQuery).
		From(int(esQuery.GetFrom())).
		Size(size).
		Do(s.ctx)
	if err != nil {
		return nil, err
	}
	branches := make([]*account.Branch, 0)
	if searchResult.Hits.TotalHits > 0 {
		// Iterate through results
		for _, hit := range searchResult.Hits.Hits {
			// hit.Index contains the name of the index
			var branch *account.Branch
			// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
			err := json.Unmarshal(*hit.Source, &branch)
			if err == nil {
				branches = append(branches, branch)
			}
		}
	}
	return branches, nil
}

// AddIndexBranch ss
func (s *SearchMachine) AddIndexBranch(branch *account.Branch) error {
	_, err := s.client.Index().
		Index(_branch).
		Type(_type).
		Id(branch.GetId()).
		BodyJson(branch).
		Do(s.ctx)
	return err
}

// UpdateIndexBranch any
func (s *SearchMachine) UpdateIndexBranch(branch *account.Branch) error {
	_, err := s.client.Update().
		Index(_branch).
		Type(_type).
		Id(branch.GetId()).
		Doc(&branch).Do(s.ctx)
	return err
}

// DeleteIndexBranch any
func (s *SearchMachine) DeleteIndexBranch(id string) error {
	_, err := s.client.Delete().
		Index(_branch).
		Type(_type).
		Id(id).
		Do(s.ctx)
	return err
}
