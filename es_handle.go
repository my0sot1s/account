package main

import (
	"context"
	"encoding/json"
	"os"

	"gitlab.com/my0sot1s/header/account"
	"gitlab.com/my0sot1s/helper"
)

func startIndexES() {
	core, err := InitCore()
	if err != nil {
		panic(err)
	}
	// index Branch
	branch, err := core.ReadBranches(context.Background(), &account.BranchRequest{
		Limit: 10000,
	})
	if err != nil {
		panic(err)
	}
	for _, b := range branch.GetBranches() {
		helper.Log("start index branch: ", b.GetId())
		if err := core.search.AddIndexBranch(b); err != nil {
			helper.Log(err)
		}
	}
	acc, err := core.ReadAccounts(context.Background(), &account.AccountRequest{
		Limit: 10000,
	})
	if err != nil {
		panic(err)
	}
	for _, a := range acc.GetAccounts() {
		helper.Log("start index account: ", a.GetId())
		if err := core.search.AddIndexAccount(a); err != nil {
			helper.Log(err)
		}
	}

}
func removeMappingEs() {
	core, err := InitCore()
	if err != nil {
		panic(err)
	}
	indexPath := make(map[string]string)
	if err := json.Unmarshal([]byte(os.Getenv("ES_INDEX_PATH")), &indexPath); err != nil {
		panic(err)
	}
	for k := range indexPath {
		helper.Log("remove index ", k)
		if err := core.search.DeleteIndex(k); err != nil {
			helper.Log(err)
		}
	}
}
