package main

import (
	"encoding/json"
	"errors"
	"net"
	"os"
	"time"

	"gitlab.com/my0sot1s/account/es"
	"gitlab.com/my0sot1s/helper"

	"gitlab.com/my0sot1s/account/mailsock"

	"gitlab.com/my0sot1s/account/db"
	"gitlab.com/my0sot1s/auth/hashing"
	"gitlab.com/my0sot1s/header/account"
	"gitlab.com/my0sot1s/redite"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type IDB interface {
	ReadAccount(*account.AccountRequest) (*account.Account, error)
	ReadAccounts(*account.AccountRequest) ([]*account.Account, error)
	UpsertAccount(*account.Account) (*account.Account, error)
	DeleteAccount(string) error
	ReadEmployee(*account.EmployeeRequest) (*account.Employee, error)
	ReadEmployees(*account.EmployeeRequest) ([]*account.Employee, error)
	UpsertEmployee(*account.Employee) (*account.Employee, error)
	DeleteEmployee(string) error
	ReadBranch(*account.BranchRequest) (*account.Branch, error)
	ReadBranches(*account.BranchRequest) ([]*account.Branch, error)
	UpsertBranch(*account.Branch) (*account.Branch, error)
	DeleteBranch(string) error
}

type ICache interface {
	SetValue(string, string, time.Duration) error
	GetValue(string) (string, error)
	DelKey([]string) (int, error)
	LPushItem(string, int, ...interface{}) error
	LRangeAll(string) ([]map[string]interface{}, error)
	SetExpired(string, int) bool
}

// ISearchMachine is defination of method search with es
type ISearchMachine interface {
	DeleteIndex(string) error
	SearchIndexBranchs(*account.BranchRequest) ([]*account.Branch, error)
	AddIndexBranch(*account.Branch) error
	UpdateIndexBranch(*account.Branch) error
	DeleteIndexBranch(string) error
	SearchIndexAccounts(*account.AccountRequest) ([]*account.Account, error)
	AddIndexAccount(*account.Account) error
	UpdateIndexAccount(*account.Account) error
	DeleteIndexAccount(string) error
}

// InitDB just init
func InitDB(dbURL, dbName string) (*db.DB, error) {
	dbx := &db.DB{}
	err := dbx.InitDB(dbName, dbURL)
	return dbx, err
}

// InitCache just init
func InitCache(rdHost, rdPw string) (*redite.RedisCli, error) {
	rd := &redite.RedisCli{}
	err := rd.InitRd(rdHost, rdPw)
	return rd, err
}

// InitSearch just init
func InitSearch(url string, indexPath map[string]string) (*es.SearchMachine, []error) {
	search := &es.SearchMachine{}
	helper.Log(indexPath)
	errs := search.InitElasticConfig(url, indexPath)
	return search, errs
}

// InitGrpc create auth server
func InitGrpc(core *Account, port string) error {
	listen, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}
	serve := grpc.NewServer()
	account.RegisterAccountRPCServer(serve, core)
	reflection.Register(serve)
	return serve.Serve(listen)
}

// InitCore service
func InitCore() (*Account, error) {
	db, err := InitDB(os.Getenv("DB_URL"), os.Getenv("DB_NAME"))
	if err != nil {
		return nil, err
	}
	cache, err := InitCache(os.Getenv("RD_HOST"), os.Getenv("RD_PW"))
	if err != nil {
		return nil, err
	}

	indexPath := make(map[string]string)
	if err := json.Unmarshal([]byte(os.Getenv("ES_INDEX_PATH")), &indexPath); err != nil {
		return nil, err
	}
	search, errs := InitSearch(os.Getenv("ES_URL"), indexPath)
	if len(errs) != 0 {
		helper.Log(indexPath, errs)
		return nil, errors.New("ES can not start")
	}
	mailGun := &mailsock.MailSock{}
	mailGun.InitEmail(os.Getenv("MAIL_KEY"), os.Getenv("MAIL_HOST"))
	return &Account{
		db:      db,
		cache:   cache,
		search:  search,
		hash:    &hashing.Hash{},
		mailGun: mailGun,
	}, nil
}
