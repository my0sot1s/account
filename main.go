package main

import (
	"os"

	"github.com/urfave/cli"
	"gitlab.com/my0sot1s/helper"
)

func main() {
	helper.Log(helper.TitleConsole("H e l l o   a c c o u n t "))
	if err := startApp(configApp()); err != nil {
		helper.ErrLog(err)
		panic(err)
	}
}

func configApp() *cli.App {
	app := cli.NewApp()
	app.Action = func(c *cli.Context) error {
		helper.Log("Wow, Do you know my name??")
		return nil
	}
	return app
}

func startApp(app *cli.App) error {
	app.Commands = []cli.Command{
		{
			Name: "start",
			Action: func(ctx *cli.Context) {
				core, err := InitCore()
				if err != nil {
					panic(err)
				}
				if err := InitGrpc(core, ":"+os.Getenv("PORT")); err != nil {
					panic(err)
				}
			},
		},
		{
			Name:   "addEs",
			Action: func(ctx *cli.Context) { startIndexES() },
		},
		{
			Name:   "removeEs",
			Action: func(ctx *cli.Context) { removeMappingEs() },
		},
	}
	return app.Run(os.Args)
}
